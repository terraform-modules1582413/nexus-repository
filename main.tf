variable "tolerations_mapping" {
  default = {
    "NO_SCHEDULE"        = "NoSchedule",
    "PREFER_NO_SCHEDULE" = "PreferNoSchedule",
    "NO_EXECUTE"         = "NoExecute"
  }
}

locals {
  ingress_domain = "${module.name.pretty}.${var.cf_base_domain}"
  tolerations = [
    for taint in var.nexus_taints : {
      key      = taint.key
      value    = taint.value
      effect   = lookup(var.tolerations_mapping, taint.effect)
      operator = "Equal"
    }
  ]
}

resource "kubernetes_secret" "repository" {
  metadata {
    name      = "nexus-repo-${var.argocd_project}"
    namespace = var.argocd_namespace
    labels = {
      "argocd.argoproj.io/secret-type" = "repository"
    }
  }
  data = {
    project = var.argocd_project
    type    = "git"
    url     = var.nexus_repo_url
  }
  type = "Opaque"
}

resource "time_sleep" "wait_10_seconds" {
  depends_on = [kubernetes_secret.repository]

  create_duration = "10s"
}

resource "kubernetes_manifest" "argocd_application" {
  manifest = yamldecode(
    templatefile("${path.module}/argocd-application.yaml", {
      argocd_name      = "nexus-${var.argocd_project}"
      argocd_namespace = var.argocd_namespace
      argocd_project   = var.argocd_project
      environment      = var.project_environment
      tolerations      = local.tolerations
      repo             = var.nexus_repo_url
      namespace        = var.nexus_namespace
      crt_name         = var.nexus_crt_name
      crt              = var.nexus_crt
      crt_key          = var.nexus_crt_key
      domain           = local.ingress_domain
      ingress_class    = var.ingress_class
    })
  )
  depends_on = [kubernetes_secret.repository, time_sleep.wait_10_seconds]
}

resource "cloudflare_record" "nexus" {
  zone_id = data.cloudflare_zone.main.zone_id
  name    = module.name.pretty
  value   = var.ingress_public_ip
  type    = "A"
  ttl     = "300"
  proxied = false
}
