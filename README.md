<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 4.0.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.9.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.18.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | ~> 4.0.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | ~> 2.18.1 |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_name"></a> [name](#module\_name) | git::https://gitlab.com/terraform-modules1582413/name.git | n/a |

## Resources

| Name | Type |
|------|------|
| [cloudflare_record.nexus](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [kubernetes_manifest.argocd_application](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_secret.repository](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [time_sleep.wait_10_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |
| [cloudflare_zone.main](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/data-sources/zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_argocd_namespace"></a> [argocd\_namespace](#input\_argocd\_namespace) | n/a | `string` | `"argocd"` | no |
| <a name="input_argocd_project"></a> [argocd\_project](#input\_argocd\_project) | n/a | `string` | `"build"` | no |
| <a name="input_cf_base_domain"></a> [cf\_base\_domain](#input\_cf\_base\_domain) | n/a | `string` | n/a | yes |
| <a name="input_ingress_class"></a> [ingress\_class](#input\_ingress\_class) | n/a | `string` | n/a | yes |
| <a name="input_ingress_public_ip"></a> [ingress\_public\_ip](#input\_ingress\_public\_ip) | n/a | `string` | n/a | yes |
| <a name="input_nexus_crt"></a> [nexus\_crt](#input\_nexus\_crt) | n/a | `string` | n/a | yes |
| <a name="input_nexus_crt_key"></a> [nexus\_crt\_key](#input\_nexus\_crt\_key) | n/a | `string` | n/a | yes |
| <a name="input_nexus_crt_name"></a> [nexus\_crt\_name](#input\_nexus\_crt\_name) | n/a | `string` | n/a | yes |
| <a name="input_nexus_namespace"></a> [nexus\_namespace](#input\_nexus\_namespace) | n/a | `string` | `"build"` | no |
| <a name="input_nexus_repo_url"></a> [nexus\_repo\_url](#input\_nexus\_repo\_url) | n/a | `string` | n/a | yes |
| <a name="input_nexus_taints"></a> [nexus\_taints](#input\_nexus\_taints) | n/a | <pre>list(object({<br>    key    = string<br>    value  = string<br>    effect = string<br>  }))</pre> | `[]` | no |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_project_prefix"></a> [project\_prefix](#input\_project\_prefix) | n/a | `string` | n/a | yes |
| <a name="input_tolerations_mapping"></a> [tolerations\_mapping](#input\_tolerations\_mapping) | n/a | `map` | <pre>{<br>  "NO_EXECUTE": "NoExecute",<br>  "NO_SCHEDULE": "NoSchedule",<br>  "PREFER_NO_SCHEDULE": "PreferNoSchedule"<br>}</pre> | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->