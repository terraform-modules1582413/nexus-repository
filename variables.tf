variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}

variable "argocd_namespace" {
  type    = string
  default = "argocd"
}
variable "argocd_project" {
  type    = string
  default = "build"
}

variable "ingress_public_ip" {
  type = string
}
variable "ingress_class" {
  type = string
}

variable "cf_base_domain" {
  type = string
}

variable "nexus_taints" {
  type = list(object({
    key    = string
    value  = string
    effect = string
  }))
  default = []
}
variable "nexus_namespace" {
  type    = string
  default = "build"
}
variable "nexus_repo_url" {
  type = string
}
variable "nexus_crt_name" {
  type      = string
  sensitive = true
}
variable "nexus_crt" {
  type      = string
  sensitive = true
}
variable "nexus_crt_key" {
  type      = string
  sensitive = true
}
